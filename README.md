# IOWseq000046_sponge_microbiome

16S amplicon sequence data analysis for Master thesis project of Philipp Hoy (supervisor Klaus Jürgens).

In this study, we accessed the microbiome of 40 *Halichondria panicea* individuals from four locations at the Baltic Sea. The main goal was to investigate how the prokaryotic community inside the sponges responds to changes in salinity across the salinity gradient of the Baltic Sea.
For this purpose, sponge tissues and seawater were sampled by scientific divers on 2018-05-18 in Sweden (1 location) and between 2023-04-04 and 2023-04-23 in German waters (3 locations). At each site, 10 individuals were sampled by cutting a piece of tissue about the size of a thumb. Three seawater samples were taken per site in close proximity to the sponge colonies. 
The temperature of the bottom water was measured with the divers' dive computers. The salinity was measured with a salinity probe in the remaining volume of the seawater samples that were not used for DNA extraction.
The sponge tissue was cleaned of visible contaminants such as ingrown algae or mussels and rinsed in sterile seawater before DNA extraction using the DNeasy PowerSoil Pro Kit (Qiagen, Hilden, Germany). Input for DNA extraction was 100-200 mg of wet tissue.
For DNA extraction of the seawater samples, 500 mL of each sample, collected on 0.22 µm pore-size polycarbonate filters, was used utilizing the DNeasy Mini Kit (Qiagen, Hilden, Germany).
The V3-V4 hypervariable region of the 16S ribosomal RNA gene was PCR amplified using primers 341F (3'-CCTAYGGGRBGCASCAG-5') and 806R (3'-GGACTACNNGGGTATCTAAT-5') (Sundberg et al., 2013) and sequenced in a 2x300 bp paired-end run on the MiSeq Illumina platform at LGC Genomics (Berlin, Germany).
The copy numbers of the 16S rRNA gene for *Ca.* Halichondribacter symbioticus and for the total bacterial community were determined by droplet digital PCR (ddPCR). The universal bacterial primers E1052f (5’-TGCATGGYTGTCGTCAGCTCG-3’) and E1193r (5’- CGTCRTCCCCRCCTTCC-3’) and the primers Hal Sym F (5’-CGCGGATGGTAGAGATACCG-3’) and Hal Sym R (5’-TGTCCCCAACTGAATGCTGG-3’), highly specific for *Ca.* Halichondribacter symbioticus, were used (Schmittmann & Pita, 2022). The ddPCR was performed on a QX200 Droplet Digital System by Biorad (USA) using the EvaGreen Assay.


#### Step 1: Extract sample list from sample_metadata

```
cd /bio/Analysis_data/IOWseq000046_sponge_microbiome/Intermediate_results/Repos/IOWseq000046_sponge_microbiome
cut -f37,38,39 /bio/Raw_data/IOWseq000046_sponge_microbiome/sample_metadata.tsv | sed '1d' > sample_list.txt
```

Check that there is no empty line at the end of the sample list file. Number of lines corresponds to number of samples.


#### Step 2: Prepare workflow configuration

```
cd /bio/Analysis_data/IOWseq000046_sponge_microbiome/Intermediate_results/Repos/IOWseq000046_sponge_microbiome
cp /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/config/config.yaml ./
vi config.yaml
cp /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/Snakefile_trimming ./
cp /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/Snakefile_denoising ./
cp /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/Snakefile_classification ./
# modify path to config file

```

#### Step: Run workflow

```
cd /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/
conda activate snakemake

# 1: Primer clipping and parameter screening for filtering settings (-np for dry run)
snakemake -s /bio/Analysis_data/IOWseq000046_sponge_microbiome/Intermediate_results/Repos/IOWseq000046_sponge_microbiome/Snakefile_trimming --use-conda --cores 60 -np

# 2: Denoising
snakemake -s /bio/Analysis_data/IOWseq000046_sponge_microbiome/Intermediate_results/Repos/IOWseq000046_sponge_microbiome/Snakefile_denoising --use-conda --cores 60 -np

# 3: Taxonomic classification
snakemake -s /bio/Analysis_data/IOWseq000046_sponge_microbiome/Intermediate_results/Repos/IOWseq000046_sponge_microbiome/Snakefile_classification --use-conda --cores 60 -np
```

#### Step 4: Inspection and additional quality control of the denoised amplicons

Run R script [R_script_MT_QC](https://git.io-warnemuende.de/bio_inf/IOWseq000046_sponge_microbiome/src/branch/master/R_script_MT_QC.R) for quality control of 16S rRNA gene amplicon sequencing data and export of quality controlled data.


#### Step 5: Statistical analysis and visualiation of the amplicon sequencing data

Run R script [R_script_MT_analysis](https://git.io-warnemuende.de/bio_inf/IOWseq000046_sponge_microbiome/src/branch/master/R_script_MT_analysis.R) for analysis and visualization of 16S rRNA gene amplicon sequencing data.


#### Step 6: Statistical analysis and visualization of ddPCR data

Run R script [R_script_MT_ddPCR_analysis](https://git.io-warnemuende.de/bio_inf/IOWseq000046_sponge_microbiome/src/branch/master/R_script_MT_ddPCR_analysis.R) for analysis and visualization of the ddPCR data.


#### Data files provided in the repository

The FASTA file [COI_sponge_host_alignment.fas](https://git.io-warnemuende.de/bio_inf/IOWseq000046_sponge_microbiome/src/branch/master/COI_sponge_host_alignment.fas) contains partial COI sequences (628 bp) for each analyzed sponge individual. The partial COI sequences are aligned against the reference sequence "[NC_040168.1](https://www.ncbi.nlm.nih.gov/nuccore/NC_040168.1/) Halichondria panicea mitochondrion, complete genome" from Genbank.

The FASTA file [Hal_Sym_alignment.fas](https://git.io-warnemuende.de/bio_inf/IOWseq000046_sponge_microbiome/src/branch/master/Hal_Sym_alignment.fas) contains partial 16S rRNA gene sequences of *Ca.* Halichondribacter symbioticus retrieved from four analyzed sponge indivduals.
The fragments were amplified with the same primer set (Hal Sym F + Hal Sym R) that were used in the ddPCR.
The fragments are aligned against the reference sequence "[MH734183.1](https://www.ncbi.nlm.nih.gov/nuccore/MH734183.1/) Candidatus Halichondribacter symbioticus clone Hp1 16S ribosomal RNA gene, partial sequence" from Genbank.
