##########################
# Analysis of ddPCR data #
##########################

#This script is for analysis and visualization of ddPCR data generated within the Master´s thesis of Philipp Hoy
# investigating the microbiome of demosponge "Halichondria panicea" along the salinity gradient of the Baltic sea.

### prepare environment ####

# set working directory
setwd("C:/Users/phili/OneDrive/Dokumente/Studium/Master Meeresbiologie/Masterarbeit/R_script_clean_up_working_directory")

# load packages
require(ggplot2)
# ggplot2_3.5.1
require(cowplot)
# cowplot_1.1.3
require(scales)
# scales_1.3.0
require(FSA)
# FSA_0.9.5
require(rcompanion)
# rcompanion_2.4.36

# loading and saving workspace
load("2024-04-15_R_script_MT_ddPCR_analysis.Rdata")
save.image("2024-04-15_R_script_MT_ddPCR_analysis.Rdata")

### Read data ####

# ddPCR_data_mod
ddPCR_data_mod <- read.table(
  "data_ddPCR_R_mod_tissue.txt",
  h = T,
  sep = "\t"
)

ddPCR_data_mod$location <- factor(
  ddPCR_data_mod$location,
  levels = c("Vattenholmen", "Fahrensodde", "Schilksee", "Nienhagen")
)

### plot boxplots ####

# boxplot copy numbers
bp_copies_all <- ggplot(ddPCR_data_mod, aes(x = location, y = copies_mg_tissue, fill = type_copies)) +
  geom_boxplot(linewidth = 0.2, fatten = 2) +
  # change color and names
  scale_fill_manual(
    values = c("steelblue2", "indianred3"),
    labels = c("Halichondribacter", "universal bacterial")
  ) +  
  # axis labels with formatting
  labs(y = "gene copies per mg sponge tissue", x = "") +                                  
  theme(
    axis.title = element_text(face = "bold"),
    axis.title.y = element_text(margin = margin(r = 5)),
    axis.text.y = element_text(angle = 90),
    axis.text.x = element_blank(),
    axis.ticks.x = element_blank()
  ) +
  # groups inside the plot
  facet_grid(~ location, switch = "x", scales = "free", space = "free") + 
  theme(
    panel.spacing = unit(0, "lines"),
    strip.background = element_rect(colour = "black", fill = "white")
  ) +
  # legend 
  theme(                               
    legend.title = element_blank(),
    legend.position = c(0.25, 0.88),
    # font size
    text = element_text(size = 8.5)
  )

# show plot
bp_copies_all  

# boxplot % Halichondribacter of total bacterial abundance
bp_perc_Hal <- ggplot(ddPCR_data_mod, aes(x = location, y = perc_hal_univ)) +
  geom_boxplot(linewidth = 0.2, fatten = 2, fill = "darkgrey") +
  #axis labels with formatting
  labs(y = "proportion of Halichondribacter specific copies in %       ", x = "") +                                  
  theme(
    axis.title = element_text(face = "bold"),
    axis.title.y = element_text(margin = margin(r = 5)),
    axis.text.x = element_blank(),
    axis.ticks.x = element_blank()
  ) +
  # groups inside the plot
  facet_grid(~ location, switch = "x", scales = "free", space = "free") + 
  theme(
    panel.spacing = unit(0, "lines"),
    strip.background = element_rect(colour = "black", fill = "white"),
    # font size
    text = element_text(size = 8.5)
  )

# show plot 
bp_perc_Hal   

# merge bp_copies_all and bp_perc_Hal
bp_ddPCR <- plot_grid(bp_copies_all, bp_perc_Hal, labels = c("A", "B"), ncol = 2, nrow = 1)

# show plot
bp_ddPCR  

# !!! dont forget to add cld in PDF editor !!!

### ddPCR data stats ####

# create vector with "type_copies"
stat_ddPCR_tissue <- c("hal", "univ")

# ANOVA
mod1_aov_tissue <- lapply(
  stat_ddPCR_tissue,
  function(y) {
    aov(log10(ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "copies_mg_tissue"]) ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "location"])
  }
)

par(mfrow = c(2, 3))
lapply(
  mod1_aov_tissue,
  function(y) {
    plot(resid(y) ~ fitted(y))
  }
)

lapply(
  mod1_aov_tissue,
  function(y) {
    shapiro.test(resid(y))
  }
)
# [[1]]
# 
# Shapiro-Wilk normality test
# 
# data:  resid(y)
# W = 0.98216, p-value = 0.769
# 
# 
# [[2]]
# 
# Shapiro-Wilk normality test
# 
# data:  resid(y)
# W = 0.92596, p-value = 0.01191

# For "univ" dataset no normal distribution => Kruskal-Wallis test  

# Kruskal-Wallis test 
lapply(
  stat_ddPCR_tissue,
  function(y) {
    kruskal.test(ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "copies_mg_tissue" ] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "location"])
  }
)
# [[1]]
# 
# Kruskal-Wallis rank sum test
# 
# data:  ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "copies_mg_tissue"] by ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "location"]
# Kruskal-Wallis chi-squared = 20.807, df = 3, p-value = 0.0001155
# 
# 
# [[2]]
# 
# Kruskal-Wallis rank sum test
# 
# data:  ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "copies_mg_tissue"] by ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "location"]
# Kruskal-Wallis chi-squared = 26.756, df = 3, p-value = 6.624e-06


# There is a significant difference in both data sets 

# Dunn test
lapply(
  stat_ddPCR_tissue,
  function(y){
    dunnTest(
      ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "copies_mg_tissue"] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == y, "location"],
      data = ddPCR_data_mod,
      method ="bonferroni"
    )
  }
)

# cld for "hal" 
dunn_hal <- dunnTest(
  ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "copies_mg_tissue"] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "location"],
  data = ddPCR_data_mod,
  method ="bonferroni"
)

cldList(
  P.adj ~ Comparison,
  data = dunn_hal$res,
  threshold = 0.05,
  remove.zero = FALSE
)
# cldList(
#   P.adj ~ Comparison,
#   data = dunn_hal$res,
#   threshold = 0.05,
#   remove.zero = FALSE
# )
# Group Letter MonoLetter
# 1  Fahrensodde      a         a 
# 2    Nienhagen      b          b
# 3    Schilksee      b          b
# 4 Vattenholmen      b          b

# cld for "univ" 
dunn_univ <- dunnTest(
  ddPCR_data_mod[ddPCR_data_mod$type_copies == "univ", "copies_mg_tissue"] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == "univ", "location"],
  data = ddPCR_data_mod,
  method ="bonferroni"
)

cldList(
  P.adj ~ Comparison,
  data = dunn_univ$res,
  threshold = 0.05,
  remove.zero = FALSE
)
# cldList(
#   P.adj ~ Comparison,
#   data = dunn_univ$res,
#   threshold = 0.05,
#   remove.zero = FALSE
# )
# Group Letter MonoLetter
# 1  Fahrensodde     ab        ab 
# 2    Nienhagen      a        a  
# 3    Schilksee     bc         bc
# 4 Vattenholmen      c          c

### ddPCR data stats percent Halichondribacter ####

# ANOVA 
mod1_aov_perc_hal_univ <- aov(
  log10(ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "perc_hal_univ"]) ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "location"]
)
  
par(mfrow = c(2, 3))

plot(resid(mod1_aov_perc_hal_univ) ~ fitted(mod1_aov_perc_hal_univ))

shapiro.test(resid(mod1_aov_perc_hal_univ))
# Shapiro-Wilk normality test
# 
# data:  resid(mod1_aov_perc_hal_univ)
# W = 0.92493, p-value = 0.01105

# no normal distribution in this dataset => Kruskal-Wallis test !

# Kruskal-Wallis test
kruskal.test(
  ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "perc_hal_univ"] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "location"]
)
# Kruskal-Wallis rank sum test
# data:  ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "perc_hal_univ"] by ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "location"]
# Kruskal-Wallis chi-squared = 21.054, df = 3, p-value = 0.0001026

# there is a significant difference in this dataset

# Dunn test + cld for "perc_hal_univ" 
dunn_perc_hal_univ <- dunnTest(
  ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "perc_hal_univ"] ~ ddPCR_data_mod[ddPCR_data_mod$type_copies == "hal", "location"],
  data = ddPCR_data_mod,
  method = "bonferroni"
)

cldList(
  P.adj ~ Comparison,
  data = dunn_perc_hal_univ$res,
  threshold = 0.05,
  remove.zero = FALSE
)
# Group Letter MonoLetter
# 1  Fahrensodde      a         a 
# 2    Nienhagen      b          b
# 3    Schilksee      a         a 
# 4 Vattenholmen      a         a 

### values of interest for results ####

# vector with names of locations
locations <- levels(ddPCR_data_mod$location)

# values of interest for Halichondribacter
# range
lapply(
  locations,
  function(x){
    range(ddPCR_data_mod[ddPCR_data_mod$location == x & ddPCR_data_mod$type_copies == "hal", "copies_mg_tissue"])
  }
)

# median
lapply(
  locations,
  function(x){
    median(ddPCR_data_mod[ddPCR_data_mod$location == x & ddPCR_data_mod$type_copies == "hal", "copies_mg_tissue"])
  }
)

# for total bacterial community
# range
lapply(
  locations,
  function(x){
    range(ddPCR_data_mod[ddPCR_data_mod$location == x & ddPCR_data_mod$type_copies == "univ", "copies_mg_tissue"])
  }
)

# median
lapply(
  locations,
  function(x){
    median(ddPCR_data_mod[ddPCR_data_mod$location == x & ddPCR_data_mod$type_copies == "univ", "copies_mg_tissue"])
  }
)
